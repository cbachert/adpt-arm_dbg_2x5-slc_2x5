EESchema Schematic File Version 4
LIBS:ADPT-ARM_DBG_2x5-SLC_2x5-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x05_Counter_Clockwise J3
U 1 1 5D50BC8D
P 7450 2000
F 0 "J3" H 7500 2417 50  0000 C CNN
F 1 "Conn_02x05_Counter_Clockwise" H 7500 2326 50  0000 C CNN
F 2 "SLC_2x5_0.68x16.55.pretty:SLC_2x5_0.68x16.55" H 7450 2000 50  0001 C CNN
F 3 "~" H 7450 2000 50  0001 C CNN
	1    7450 2000
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J1
U 1 1 5D50DD3D
P 4950 2000
F 0 "J1" H 5000 2417 50  0000 C CNN
F 1 "Conn_02x05_Odd_Even" H 5000 2326 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical" H 4950 2000 50  0001 C CNN
F 3 "~" H 4950 2000 50  0001 C CNN
	1    4950 2000
	1    0    0    -1  
$EndComp
Text Label 7250 1800 2    50   ~ 0
VCCS
Text Label 7250 1900 2    50   ~ 0
TMS
Text Label 7250 2000 2    50   ~ 0
GND
Text Label 7250 2100 2    50   ~ 0
TCK
Text Label 7250 2200 2    50   ~ 0
VKS
Text Label 7750 1900 0    50   ~ 0
TRST
Text Label 7750 2000 0    50   ~ 0
TDI
Text Label 7750 2100 0    50   ~ 0
RTCK
Text Label 7750 2200 0    50   ~ 0
TDO
Text Label 5250 2200 0    50   ~ 0
RES
Text Label 5250 2100 0    50   ~ 0
TDI
Text Label 5250 2000 0    50   ~ 0
TDO
Text Label 5250 1900 0    50   ~ 0
TCK
Text Label 5250 1800 0    50   ~ 0
TMS
Text Label 4750 2200 2    50   ~ 0
TRST
Text Label 4750 2100 2    50   ~ 0
RTCK
Text Label 4750 2000 2    50   ~ 0
VKS
Text Label 4750 1800 2    50   ~ 0
VCCS
Wire Wire Line
	5250 1800 5450 1800
Wire Wire Line
	5250 1900 5450 1900
Wire Wire Line
	5250 2000 5450 2000
Wire Wire Line
	5250 2100 5450 2100
Wire Wire Line
	4750 1900 4550 1900
Wire Wire Line
	4750 2000 4550 2000
Wire Wire Line
	4750 2100 4550 2100
Wire Wire Line
	4750 2200 4550 2200
Entry Wire Line
	4450 1700 4550 1800
Wire Wire Line
	5250 2200 5450 2200
Wire Wire Line
	4550 1800 4750 1800
Entry Wire Line
	4450 1800 4550 1900
Entry Wire Line
	4450 1900 4550 2000
Entry Wire Line
	4450 2000 4550 2100
Entry Wire Line
	4450 2100 4550 2200
Entry Wire Line
	5450 1800 5550 1900
Entry Wire Line
	5450 1900 5550 2000
Entry Wire Line
	5450 2000 5550 2100
Entry Wire Line
	5450 2100 5550 2200
Entry Wire Line
	5450 2200 5550 2300
Wire Wire Line
	7250 1800 7050 1800
Wire Wire Line
	7250 1900 7050 1900
Wire Wire Line
	7250 2000 7050 2000
Wire Wire Line
	7250 2100 7050 2100
Wire Wire Line
	7250 2200 7050 2200
Wire Wire Line
	7750 1800 7950 1800
Wire Wire Line
	7750 1900 7950 1900
Wire Wire Line
	7750 2000 7950 2000
Wire Wire Line
	7750 2100 7950 2100
Wire Wire Line
	7750 2200 7950 2200
Text Label 7750 1800 0    50   ~ 0
RES
Entry Wire Line
	7950 1800 8050 1900
Entry Wire Line
	7950 1900 8050 2000
Entry Wire Line
	7950 2000 8050 2100
Entry Wire Line
	7950 2100 8050 2200
Entry Wire Line
	7950 2200 8050 2300
Entry Wire Line
	6950 1700 7050 1800
Entry Wire Line
	6950 1800 7050 1900
Entry Wire Line
	6950 1900 7050 2000
Entry Wire Line
	6950 2000 7050 2100
Entry Wire Line
	6950 2100 7050 2200
Entry Bus Bus
	4350 1400 4450 1500
Entry Bus Bus
	5450 1400 5550 1500
Entry Bus Bus
	6850 1400 6950 1500
Entry Bus Bus
	7950 1400 8050 1500
Text Label 4600 1900 0    50   ~ 0
GND
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J2
U 1 1 5D50C699
P 4950 2900
F 0 "J2" H 5000 3317 50  0000 C CNN
F 1 "Conn_02x05_Odd_Even" H 5000 3226 50  0000 C CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_2x05_P1.27mm_Vertical" H 4950 2900 50  0001 C CNN
F 3 "~" H 4950 2900 50  0001 C CNN
	1    4950 2900
	1    0    0    -1  
$EndComp
Text Label 5250 3100 0    50   ~ 0
RES
Text Label 5250 3000 0    50   ~ 0
TDI
Text Label 5250 2900 0    50   ~ 0
TDO
Text Label 5250 2800 0    50   ~ 0
TCK
Text Label 5250 2700 0    50   ~ 0
TMS
Text Label 4750 3100 2    50   ~ 0
TRST
Text Label 4750 3000 2    50   ~ 0
RTCK
Text Label 4750 2900 2    50   ~ 0
VKS
Text Label 4750 2700 2    50   ~ 0
VCCS
Wire Wire Line
	5250 2700 5450 2700
Wire Wire Line
	5250 2800 5450 2800
Wire Wire Line
	5250 2900 5450 2900
Wire Wire Line
	5250 3000 5450 3000
Wire Wire Line
	4750 2800 4550 2800
Wire Wire Line
	4750 2900 4550 2900
Wire Wire Line
	4750 3000 4550 3000
Wire Wire Line
	4750 3100 4550 3100
Entry Wire Line
	4450 2600 4550 2700
Wire Wire Line
	5250 3100 5450 3100
Wire Wire Line
	4550 2700 4750 2700
Entry Wire Line
	4450 2700 4550 2800
Entry Wire Line
	4450 2800 4550 2900
Entry Wire Line
	4450 2900 4550 3000
Entry Wire Line
	4450 3000 4550 3100
Entry Wire Line
	5450 2700 5550 2800
Entry Wire Line
	5450 2800 5550 2900
Entry Wire Line
	5450 2900 5550 3000
Entry Wire Line
	5450 3000 5550 3100
Entry Wire Line
	5450 3100 5550 3200
Text Label 4600 2800 0    50   ~ 0
GND
Wire Bus Line
	4250 1400 8050 1400
Wire Bus Line
	4450 1500 4450 2200
Wire Bus Line
	8050 1500 8050 2400
Wire Bus Line
	6950 1500 6950 2200
Wire Bus Line
	4450 2400 4450 3100
Wire Bus Line
	5550 1500 5550 3300
$EndSCHEMATC
